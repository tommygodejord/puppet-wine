# Class: wine
# ===========================
#
# This class installes Wine and it's helper-script Winetricks
#
# Variables
# ----------
#
# [ensure]
# The different states of a package. E.i. installed, latest
#
# [pkg]
# The Wine package. Currently set to stable version
#
# [ppa]
# The Personal Package Archive to download Wine from
#
# [wt_req]
# Winetricks Required packages. These packages need to be installed for Winetricks to work.
#
# Authors
# -------
#
# Author Name Tommy Godejord
#
# Copyright
# ---------
#
# Copyright 2017 Tommy Godejord
#
class wine(
  $ensure     = 'installed',
  $pkg        = 'winehq-stable',
  $ppa        = 'https://dl.winehq.org/wine-builds/ubuntu/',
  $wt_req     = "['cabextract', 'p7zip', 'unrar', 'unzip', 'wget', 'zenity']",
) {
  include apt
  include wget

  case $facts['os']['name'] {
    'Ubuntu': {
      exec { 'dpkg --add-architecture i386':
        cwd    => '/usr/bin/dpkg',
        before => Package[$pkg],
        unless => '/urs/bin/dpkg --print-foreign-architectures | /bin/grep i386',
      }
      apt::ppa { $ppa:
        before => Package[$pkg],
      }
      apt::key { 'Release.key':
        key        => 'Release.key',
        key_source => 'https://dl.winehq.org/wine-builds/Release.key',
        before     => Package[$pkg],
      }
    }
    default: {
      $pkg = undef
      notice("$(::osfamily) is not supported by installation.")
    }
  }
  package { $pkg:
    ensure          => $ensure,
    install_options => '--install-recommends',
    require         => Exec['apt-get update'],
  }
  package { $wt_req:
    ensure  => $ensure,
    require => Package[$pkg],
  }
  wget::fetch { 'Winetricks':
    source      => 'https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks',
    destination => '/usr/local/bin/winetricks',
    timeout     => 0,
    verbose     => false,
    require     => Package[$wt_req],
    before      => Exec['winetricks chmod'],
  }
  exec { 'winetricks chmod':
    cwd     => '/usr/local/bin',
    command => 'chmod +x winetricks',
  }
}
